```
-- Running cmake version 2.8.11
-- MySQL 5.6.32-78.0
-- Packaging as: percona-server-5.6.32-78.0-Linux-x86_64
-- HAVE_VISIBILITY_HIDDEN
-- HAVE_VISIBILITY_HIDDEN
-- HAVE_VISIBILITY_HIDDEN
-- READLINE_INCLUDE_DIR /usr/include/readline
-- READLINE_LIBRARY /usr/lib64/libreadline.so
-- Using cmake version 2.8.11
-- Not building NDB
-- tokudb-backup-plugin include backup HotBackup
-- Library perconaserverclient depends on OSLIBS -lpthread;m;rt;dl
-- Library mysqlserver depends on OSLIBS -lpthread;m;rt;crypt;dl;aio
-- CMAKE_BUILD_TYPE: RelWithDebInfo
-- COMPILE_DEFINITIONS: HAVE_CONFIG_H
-- CMAKE_C_FLAGS:  -Wall -Wextra -Wformat-security -Wvla -Wwrite-strings -Wdeclaration-after-statement
-- CMAKE_CXX_FLAGS:  -Wall -Wextra -Wformat-security -Wvla -Woverloaded-virtual -Wno-unused-parameter
-- CMAKE_C_FLAGS_RELWITHDEBINFO: -O3 -g -fabi-version=2 -fno-omit-frame-pointer -fno-strict-aliasing -DDBUG_OFF
-- CMAKE_CXX_FLAGS_RELWITHDEBINFO: -O3 -g -fabi-version=2 -fno-omit-frame-pointer -fno-strict-aliasing -DDBUG_OFF
-- Configuring done
-- Generating done
-- Build files have been written to: /usr/local/src/percona5632780
-- Cache values
// Don't set this by hand.  It's used to tell the subcmake not to set things
BACKUP_HAS_PARENT:BOOL=ON

// CTest build name
BUILDNAME:STRING=PerconaFT RelWithDebInfo Linux-3.10.0-123.9.3.el7.x86_64 x86_64 GNU c++ 4.8.5

// Build the testing tree.
BUILD_TESTING:BOOL=OFF

// Choose the type of build, options are: None(CMAKE_CXX_FLAGS or
 CMAKE_C_FLAGS used) Debug Release RelWithDebInfo MinSizeRel
CMAKE_BUILD_TYPE:STRING=RelWithDebInfo

// Install path prefix, prepended onto install directories.
CMAKE_INSTALL_PREFIX:PATH=/usr/local/mysql

// Revision of tokudb.
CMAKE_TOKUDB_REVISION:STRING=0

// Set to true if this is a community build
COMMUNITY_BUILD:BOOL=ON

// Path to a program.
COVERAGE_COMMAND:FILEPATH=/usr/bin/gcov

// Path to a program.
CSCOPE:FILEPATH=/usr/bin/cscope

// Path to a program.
CTAGS:FILEPATH=/usr/bin/ctags

// Enable profiling
ENABLED_PROFILING:BOOL=ON

// Enable debug sync (debug builds only)
ENABLE_DEBUG_SYNC:BOOL=ON

// Download and build 3rd party source code components, e.g. google mock
ENABLE_DOWNLOADS:BOOL=OFF

// Include support for DTrace probes
ENABLE_DTRACE:BOOL=OFF

// Enable gcov (debug, Linux builds only)
ENABLE_GCOV:BOOL=OFF

// Enable gprof (optimized, Linux builds only)
ENABLE_GPROF:BOOL=OFF

// Enable SASL on InnoDB Memcached
ENABLE_MEMCACHED_SASL:BOOL=OFF

// Enable SASL on InnoDB Memcached
ENABLE_MEMCACHED_SASL_PWDB:BOOL=OFF

// Path to a program.
ETAGS:FILEPATH=ETAGS-NOTFOUND

//  Selection of features. Options are
 - xsmall : 
 - small: embedded
 - classic: embedded + archive + federated + blackhole 
 - large :  embedded + archive + federated + blackhole + innodb
 - xlarge:  embedded + archive + federated + blackhole + innodb + partition
 - community:  all  features (currently == xlarge)

FEATURE_SET:STRING=community

// Path to a program.
GTAGS:FILEPATH=GTAGS-NOTFOUND

// installed name of the hot backup library
HOT_BACKUP_LIBNAME:STRING=HotBackup

// Installation directory layout. Options are: STANDALONE (as in zip or tar.gz installer), RPM, DEB, SVR4
INSTALL_LAYOUT:STRING=STANDALONE

// Where to find jemalloc sources.
JEMALLOC_SOURCE_DIR:FILEPATH=/usr/local/src/percona5632780/storage/tokudb/PerconaFT/third_party/jemalloc

// Path to a library.
JEMALLOC_STATIC_LIBRARY:FILEPATH=JEMALLOC_STATIC_LIBRARY-NOTFOUND

// Name of libtokufractaltree.so
LIBTOKUDB:STRING=tokufractaltree

// Name of libtokuportability.so
LIBTOKUPORTABILITY:STRING=tokuportability

// Sets the maximum number of indexes per table, default 64
MAX_INDEXES:STRING=64

// Path to a program.
MKID:FILEPATH=MKID-NOTFOUND

// default MySQL data directory
MYSQL_DATADIR:PATH=/usr/local/mysql/servdata

// MySQL maintainer-specific development environment
MYSQL_MAINTAINER_MODE:BOOL=OFF

// Support tracing of Optimizer
OPTIMIZER_TRACE:BOOL=ON

// Allow profiling and debug
PROFILING:BOOL=ON

// Where to find sources for snappy.
SNAPPY_SOURCE_DIR:FILEPATH=/usr/local/src/percona5632780/storage/tokudb/PerconaFT/third_party/snappy-1.1.2

// PATH to MySQL TMP dir. Defaults to the P_tmpdir macro in <stdio.h>
TMPDIR:PATH=/usr/local/mysql/servtmp

// Path to data files for tests
TOKUDB_DATA:FILEPATH=/usr/local/src/percona5632780/storage/tokudb/PerconaFT/../tokudb.data

// Enable paranoid asserts.
TOKU_DEBUG_PARANOID:BOOL=OFF

// Build the cscope database.
USE_CSCOPE:BOOL=ON

// Build the ctags database.
USE_CTAGS:BOOL=ON

// Build the etags database.
USE_ETAGS:BOOL=ON

// Use gcov for test coverage.
USE_GCOV:BOOL=OFF

// Build the gtags database.
USE_GTAGS:BOOL=ON

// Build the idutils database.
USE_MKID:BOOL=ON

// Build to run safely under valgrind (often slower).
USE_VALGRIND:BOOL=OFF

// Link ARCHIVE statically to the server
WITH_ARCHIVE_STORAGE_ENGINE:BOOL=ON

// Enable address sanitizer
WITH_ASAN:BOOL=OFF

// Link BLACKHOLE statically to the server
WITH_BLACKHOLE_STORAGE_ENGINE:BOOL=ON

// Use dbug/safemutex
WITH_DEBUG:BOOL=OFF

// Use flags from cmake/build_configurations/compiler_options.cmake
WITH_DEFAULT_COMPILER_OPTIONS:BOOL=ON

// Use feature set in cmake/build_configurations/feature_set.cmake
WITH_DEFAULT_FEATURE_SET:BOOL=ON

// Compile MySQL with embedded server
WITH_EMBEDDED_SERVER:BOOL=ON

// Generate shared version of embedded library (in addition to the static one)
WITH_EMBEDDED_SHARED_LIBRARY:BOOL=OFF

// 
WITH_EXAMPLE_STORAGE_ENGINE:BOOL=OFF

// Options are: none, complex, all
WITH_EXTRA_CHARSETS:STRING=all

// Link FEDERATED statically to the server
WITH_FEDERATED_STORAGE_ENGINE:BOOL=ON

// Link INNOBASE statically to the server
WITH_INNOBASE_STORAGE_ENGINE:BOOL=ON

// 
WITH_INNODB_MEMCACHED:BOOL=OFF

// Use bundled libevent
WITH_LIBEVENT:STRING=bundled

// Compile with tcp wrappers support
WITH_LIBWRAP:BOOL=OFF

// Explicitly set NUMA memory allocation policy
WITH_NUMA:BOOL=ON

// Build with Percona PAM plugin
WITH_PAM:BOOL=OFF

// Link PARTITION statically to the server
WITH_PARTITION_STORAGE_ENGINE:BOOL=ON

// Link PERFSCHEMA statically to the server
WITH_PERFSCHEMA_STORAGE_ENGINE:BOOL=ON

// Generate PIC objects
WITH_PIC:BOOL=OFF

// Link QUERY_RESPONSE_TIME statically to the server
WITH_QUERY_RESPONSE_TIME:BOOL=OFF

// By default use system readline
WITH_READLINE:STRING=system

// bundled (use yassl), yes (prefer os library if present, otherwise use bundled), system (use os library), </path/to/custom/installation>
WITH_SSL:STRING=bundled

// Export libmysqlclient_16 and libmysqlclient_18 symbol versions
WITH_SYMVER16:BOOL=OFF

// Compile MySQL with unit tests
WITH_UNIT_TESTS:BOOL=ON

// Valgrind instrumentation
WITH_VALGRIND:BOOL=OFF

// 
WITH_ZLIB:STRING=bundled

// Where to find sources for xz (lzma).
XZ_SOURCE_DIR:FILEPATH=/usr/local/src/percona5632780/storage/tokudb/PerconaFT/third_party/xz-4.999.9beta
```