#!/usr/bin/env bash

###################################################################################
################################## is root user ###################################
###################################################################################

if [ $(id -u) != '0' ]; then
    echo 'error: script needs root user.'
    exit 1
fi

###################################################################################
################################## install letsencrypt ############################
###################################################################################
yum -y update

cd /usr/local/src

# WARNING：fatal: Could not resolve host: github.com; Name or service not known
git clone https://github.com/letsencrypt/letsencrypt

cd letsencrypt
chmod +x letsencrypt-auto

# @see http://www.jianshu.com/p/eaac0d082ba2

./letsencrypt-auto certonly --webroot \
--agree-tos \
--email admin@admin.com \
-d example.com \
-d www.example.com \
--webroot-path /usr/local/nginx/html

# ssl_certificate     /etc/letsencrypt/live/www.example.com/fullchain.pem;
# ssl_certificate_key /etc/letsencrypt/live/www.example.com/privkey.pem;
