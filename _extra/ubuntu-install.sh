#!/usr/bin/env bash

###################################################################################
################################## is root user ###################################
###################################################################################

if [ $(id -u) != '0' ]; then
    echo 'error: script needs root user.'
    exit 1
fi


###################################################################################
################################## versions #######################################
###################################################################################

nginx_v='1.9.14'
nginx_dir='1914'

php_v='7.0.5'
php_dir='705'

mariadb_v='10.1.13'
mariadb_dir='10113'

pcre_v='8.38'
pcre_dir='838'

zlib_v='1.2.8'
zlib_dir='128'

phpmyadmin_v='4.5.5.1'
phpmyadmin_dir='4551'

nodejs_v='4.4.2'
nodejs_dir='442'

###################################################################################
################################## some env work ##################################
###################################################################################

apt-get update
apt-get upgrade

# apt-get -y install libpng libevent libmcrypt  libjpeg jemalloc bzip2-devel libcurl glibc glibc-devel glib2 glib2-devel openssl openssl-devel bison bison-devel gd flex-devel libwebp gettext-devel gmp libaio boost

apt-get -y install build-essential gcc g++ automake autoconf make cmake libssl-dev libpcre3 libpcre3-dev libtool zlib1g zlib1g-dev libfreetype libfreetype-dev libpng12-dev libevent-dev libmcrypt-dev libjpeg-dev libjemalloc-dev libxml2 libxml2-dev bzip2 libzip2 libzip-dev libcurl4-openssl-dev bison libncurses5 libncurses5-dev libgd-dev flex libwebp-dev gettext libgmp-dev libaio-dev libboost-dev libbz2-dev

ln -s /usr/include/x86_64-linux-gnu/gmp.h /usr/include/gmp.h


mkdir /usr/local/mysql
mkdir /usr/local/mysql/mydata
mkdir /usr/local/mysql/mytmp
mkdir /usr/local/mysql/mylog

mkdir /usr/local/nodejs

###################################################################################
################################## make temp swap #################################
###################################################################################

dd if=/dev/zero of=/home/swap bs=1024 count=1000000
mkswap /home/swap
swapon /home/swap


###################################################################################
################################## groups & users #################################
###################################################################################

groupadd webmaster
groupadd mysql
groupadd nodejs

useradd -g webmaster -M -d /usr/local/nginx webmaster -s /sbin/nologin
useradd -g mysql -M -d /usr/local/mysql mysql -s /sbin/nologin
useradd -g nodejs -M -d /usr/local/nodejs nodejs -s /sbin/nologin

chown -R mysql:mysql /usr/local/mysql
chown -R nodejs:nodejs /usr/local/nodejs

###################################################################################
################################## get packages ###################################
###################################################################################

cd /usr/local/src

wget http://mirrors.sohu.com/nginx/nginx-${nginx_v}.tar.gz
wget http://cn2.php.net/distributions/php-${php_v}.tar.gz
wget http://mirrors.aliyun.com/mariadb/mariadb-${mariadb_v}/source/mariadb-${mariadb_v}.tar.gz
wget https://files.phpmyadmin.net/phpMyAdmin/${phpmyadmin_v}/phpMyAdmin-${phpmyadmin_v}-all-languages.zip
wget https://npm.taobao.org/mirrors/node/v${nodejs_v}/node-v${nodejs_v}-linux-x64.tar.gz

tar -zxvf nginx-${nginx_v}.tar.gz
tar -zxvf php-${php_v}.tar.gz
tar -zxvf mariadb-${mariadb_v}.tar.gz
unzip phpMyAdmin-${phpmyadmin_v}-all-languages.zip
tar -zxvf node-v${nodejs_v}-linux-x64.tar.gz

tar -zxvf pcre-${pcre_v}.tar.gz
tar -zxvf zlib-${zlib_v}.tar.gz

mv nginx-${nginx_v}  nginx${nginx_dir}
mv php-${php_v}      php${php_dir}
mv mariadb-${mariadb_v}  mariadb${mariadb_dir}
mv node-v${nodejs_v}-linux-x64  /usr/local/nodejs/node${nodejs_dir}

# make sure nodejs base dir's owner is nodejs
chown -R nodejs:nodejs /usr/local/nodejs

mv pcre-${pcre_v}  pcre${pcre_dir}
mv zlib-${zlib_v}  zlib${zlib_dir}


###################################################################################
################################## install nginx ##################################
###################################################################################

cd /usr/local/src/nginx${nginx_dir}

./configure --prefix=/usr/local/nginx \
--user=webmaster \
--group=webmaster \
--with-pcre=/usr/local/src/pcre${pcre_dir} \
--with-zlib=/usr/local/src/zlib${zlib_dir} \
--with-pcre-jit \
--with-http_gzip_static_module \
--with-http_ssl_module \
--with-http_v2_module \
--with-ipv6 \
--with-file-aio \
--with-http_realip_module \
--with-http_sub_module \
--with-http_stub_status_module \
--with-http_random_index_module \
--with-http_flv_module \
--with-http_secure_link_module \
--with-http_mp4_module \
--with-threads \
--with-stream \
--with-stream_ssl_module \
--with-http_degradation_module
#--with-ld-opt="-ljemalloc"
#--with-http_charset_module

make && make install

# nginx.conf
mv /usr/local/nginx/conf/nginx.conf /usr/local/nginx/conf/nginx.conf.bak
cp /usr/local/src/conf/nginx.conf /usr/local/nginx/conf/nginx.conf
mv /usr/local/src/conf/vhosts /usr/local/nginx/conf/vhosts

# add phpmyadmin to nginx html dir
mv /usr/local/src/phpMyAdmin-${phpmyadmin_v}-all-languages /usr/local/nginx/html/phpmyadmin${phpmyadmin_dir}
cp /usr/local/nginx/html/phpmyadmin${phpmyadmin_dir}/config.sample.inc.php /usr/local/nginx/html/phpmyadmin${phpmyadmin_dir}/config.inc.php

# make sure nginx base dir's owner is webmaster
chown -R webmaster:webmaster /usr/local/nginx


#################################################################################
################################## install php ##################################
#################################################################################

cd /usr/local/src/php${php_dir}

./configure --prefix=/usr/local/php \
--enable-fpm \
--with-fpm-user=webmaster \
--with-fpm-group=webmaster \
--enable-libxml \
--with-libxml-dir \
--with-openssl \
--with-openssl-dir \
--with-zlib \
--with-zlib-dir \
--with-pcre-dir \
--enable-bcmath \
--with-bz2 \
--enable-calendar \
--enable-ctype \
--with-curl \
--enable-exif \
--enable-fileinfo \
--enable-filter \
--enable-ftp \
--with-jpeg-dir \
--with-png-dir \
--with-webp-dir \
--with-freetype-dir \
--with-gd \
--enable-gd-native-ttf \
--with-gettext \
--with-gmp \
--with-mhash \
--with-iconv \
--with-iconv-dir \
--enable-json \
--enable-mbstring \
--with-mcrypt \
--enable-zip \
--with-xmlrpc \
--with-mysqli \
--with-pdo-mysql \
--enable-session \
--enable-opcache \
--enable-simplexml \
--enable-soap \
--enable-sockets \
--enable-wddx

make && make install

# php conf
cp /usr/local/src/conf/php-fpm.conf  /usr/local/php/etc/php-fpm.conf
cp /usr/local/src/conf/www.conf      /usr/local/php/etc/php-fpm.d/www.conf
cp /usr/local/src/conf/php.ini       /usr/local/php/lib/php.ini

# make sure php base dir's owner is webmaster
chown -R webmaster:webmaster /usr/local/php


###################################################################################
################################## install mariadb ################################
###################################################################################

cd /usr/local/src/mariadb${mariadb_dir}

cmake -DCMAKE_INSTALL_PREFIX=/usr/local/mysql \
-DSYSCONFDIR=/etc \
-DMYSQL_DATADIR=/usr/local/mysql/mydata \
-DTMPDIR=/usr/local/mysql/mytmp \
-DSYSTEMD_PID_DIR=/usr/local/mysql/mylog \
-DMYSQL_UNIX_ADDR=/tmp/mysql.sock \
-DENABLED_LOCAL_INFILE=1 \
-DDEFAULT_CHARSET=utf8mb4 \
-DDEFAULT_COLLATION=utf8mb4_general_ci \
-DMYSQL_TCP_PORT=3306 \
-DWITH_XTRADB_STORAGE_ENGINE=1 \
-DWITH_INNOBASE_STORAGE_ENGINE=1 \
-DWITH_ARCHIVE_STORAGE_ENGINE=1 \
-DWITH_BLACKHOLE_STORAGE_ENGINE=1 \
-DWITH_PERFSCHEMA_STORAGE_ENGINE=1 \
-DWITH_EXTRA_CHARSETS=all \
-DWITH_LIBEVENT=bundled \
-DWITH_SSL=bundled \
-DWITH_ZLIB=bundled

make && make install

# mariadb conf
mv /etc/my.cnf /etc/my.cnf.bakup
cp /usr/local/src/conf/my.cnf /etc/my.cnf

# make sure mysql base dir's owner is mysql
chown -R mysql:mysql /usr/local/mysql

# initialize mariadb data
/usr/local/src/mariadb${mariadb_dir}/scripts/mysql_install_db --user=mysql \
--basedir=/usr/local/mysql \
--datadir=/usr/local/mysql/mydata

# add mysqld serivce
cp /usr/local/mysql/support-files/mysql.server /etc/init.d/mysqld
chmod +x /etc/init.d/mysqld

# make sure mysql base dir's owner is mysql again
chown -R mysql:mysql /usr/local/mysql


###################################################################################
################################## clean temp swap ################################
###################################################################################

swapoff /home/swap
rm -rf /home/swap

###################################################################################
################################## start all service ##############################
###################################################################################

/usr/local/nginx/sbin/nginx
/usr/local/php/sbin/php-fpm
service mysqld start

# make sure nginx base dir's owner is webmaster
chown -R webmaster:webmaster /usr/local/nginx
chown -R webmaster:webmaster /usr/local/php

# change mariadb password
/usr/local/mysql/bin/mysqladmin -uroot password abcd

cd
