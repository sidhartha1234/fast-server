# Default Https Server

server {
    listen       80;
    server_name  www.example.com;

    location / {
        root   /usr/local/nginx/html/certbot;
        index  index.html;
    }

    if ($request_uri !~ "^/\.well-known\/acme-challenge\/") {
        return 301 https://www.example.com$request_uri;
    }
}

server {
    listen       443 ssl http2;
    server_name  www.example.com;  # $server_name

    root   /usr/local/nginx/html; # $document_root

    ssl_certificate      /etc/letsencrypt/live/$server_name/fullchain.pem;
    ssl_certificate_key  /etc/letsencrypt/live/$server_name/privkey.pem;

    ssl_protocols       TLSv1 TLSv1.1 TLSv1.2;

    ssl_ciphers         EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH;
    ssl_prefer_server_ciphers  on;

    ssl_session_cache    shared:SSL:1m;
    ssl_session_timeout  5m;

#    add_header  'strict-transport-security' 'max-age=31536000';

    location / {
        index  index.php index.html;
        try_files  $uri $uri/ /index.php?$args;
    }

    location ~ \.(js|css|gif|jpg|png|ttf|woff)$ {
        try_files  $uri =404;
    }

    location ~ \.php$ {
        try_files      $uri =404;
        fastcgi_pass   127.0.0.1:9000;
        fastcgi_index  index.php;
        fastcgi_param  HTTP_PROXY  "";
        fastcgi_param  SCRIPT_FILENAME  $document_root$fastcgi_script_name;
        include        fastcgi_params;
    }
}
