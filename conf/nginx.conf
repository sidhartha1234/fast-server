user webmaster;
worker_processes auto;
worker_cpu_affinity auto;

# Logs
error_log  /usr/local/nginx/logs/error.log  warn;
pid        /usr/local/nginx/logs/nginx.pid;

worker_rlimit_nofile  65535;

# Events
events {
    use  epoll;
    worker_connections  4096;
    multi_accept  on;
}

# Http
http {
    include       mime.types;
    default_type  application/octet-stream;

    access_log     off;
    log_not_found  off;
    server_tokens  off;

    client_max_body_size  2m;

    sendfile            on;
    sendfile_max_chunk  128k;

    tcp_nopush   on;
    tcp_nodelay  on;

    keepalive_timeout  35;
    keepalive_disable  none;

    client_header_timeout        30;
    client_body_timeout          30;
    reset_timedout_connection    on;
    send_timeout                 30;

    gzip             on;
    gzip_min_length  2048;
    gzip_comp_level  2;
    gzip_types       text/plain text/css application/javascript application/json text/xml application/rss+xml application/atom+xml;
    gzip_proxied     off;
    gzip_vary        off;

    # Deny IP Direct Access
    server {
        listen       80 default;
        server_name  _;

        return       403;
    }

    include  /usr/local/nginx/conf/vhosts/*.conf;
}
