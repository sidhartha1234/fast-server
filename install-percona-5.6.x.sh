#!/usr/bin/env bash

if [ $(id -u) != '0' ]; then
    echo 'error: script needs root user.'
    exit 1
fi

nginxVer='1.11.3'
nginxDir='nginx1113'

phpVer='7.0.10'
phpDir='php7010'

perconaVer='5.6.32-78.0'
perconaDir='percona5632780'

redisVer='3.2.3'
redisDir='redis323'

pcreVer='8.39'
pcreDir='pcre839'

zlibVer='1.2.8'
zlibDir='zlib128'

opensslVer='1.0.2h'
opensslDir='openssl102h'

yaconfVer='1.0.2'
yaconfDir='yaconf102'

yacVer='2.0.1'
yacDir='yac201'

certbotVer='0.8.1'
certbotDir='certbot081'

yum -y update

yum -y install gcc gcc-c++ automake autoconf libtool make cmake zlib zlib-devel pcre pcre-devel freetype freetype-devel libpng libpng-devel libevent libevent-devel libmcrypt libmcrypt-devel libjpeg libjpeg-devel jemalloc jemalloc-devel libxml2 libxml2-devel bzip2 bzip2-devel libcurl libcurl-devel glibc glibc-devel glib2 glib2-devel openssl openssl-devel bison bison-devel ncurses ncurses-devel gd gd-devel flex flex-devel libwebp libwebp-devel gettext gettext-devel gmp gmp-devel libaio libaio-devel readline readline-devel

dd if=/dev/zero of=/home/swap bs=1024 count=1000000
mkswap /home/swap
swapon /home/swap

groupadd webmaster
groupadd mysql

useradd -g webmaster -M -d /usr/local/nginx webmaster -s /sbin/nologin
useradd -g mysql -M -d /usr/local/mysql mysql -s /sbin/nologin

cd /usr/local/src/fast-server

wget http://mirrors.sohu.com/nginx/nginx-${nginxVer}.tar.gz
wget http://cn2.php.net/distributions/php-${phpVer}.tar.gz
wget https://www.percona.com/downloads/Percona-Server-5.6/Percona-Server-${perconaVer}/source/tarball/percona-server-${perconaVer}.tar.gz
wget http://download.redis.io/releases/redis-${redisVer}.tar.gz
wget https://www.openssl.org/source/openssl-${opensslVer}.tar.gz

wget https://github.com/laruence/yaconf/archive/yaconf-${yaconfVer}.zip
wget https://github.com/laruence/yac/archive/yac-${yacVer}.zip

tar -zxvf nginx-${nginxVer}.tar.gz
tar -zxvf php-${phpVer}.tar.gz
tar -zxvf percona-server-${perconaVer}.tar.gz
tar -zxvf redis-${redisVer}.tar.gz
tar -zxvf openssl-${opensslVer}.tar.gz
unzip yaconf-${yaconfVer}.zip
unzip yac-${yacVer}.zip

mv nginx-${nginxVer}  ${nginxDir}
mv php-${phpVer}      ${phpDir}
mv percona-server-${perconaVer}  ${perconaDir}
mv redis-${redisVer}  ${redisDir}
mv openssl-${opensslVer}  ${opensslDir}
mv yaconf-yaconf-${yaconfVer} ${yaconfDir}
mv yac-yac-${yacVer} ${yacDir}

cd /usr/local/src/fast-server/lib
tar -zxvf pcre-${pcreVer}.tar.gz
tar -zxvf zlib-${zlibVer}.tar.gz
mv pcre-${pcreVer}  ${pcreDir}
mv zlib-${zlibVer}  ${zlibDir}

# install nginx
cd /usr/local/src/fast-server/${nginxDir}

./configure --prefix=/usr/local/nginx \
--user=webmaster \
--group=webmaster \
--with-pcre=/usr/local/src/fast-server/lib/${pcreDir} \
--with-zlib=/usr/local/src/fast-server/lib/${zlibDir} \
--with-openssl=/usr/local/src/fast-server/${opensslDir} \
--with-http_gzip_static_module \
--with-http_ssl_module \
--with-http_v2_module \
--with-file-aio \
--with-http_realip_module \
--with-http_sub_module \
--with-http_stub_status_module \
--with-http_flv_module \
--with-http_secure_link_module \
--with-http_mp4_module \
--with-threads \
--with-stream \
--with-stream_ssl_module \
--with-http_degradation_module
#--with-ld-opt="-ljemalloc"
#--with-pcre-jit \
#--with-ipv6 \
#--with-http_random_index_module \

make && make install

mkdir /usr/local/nginx/html/yaconf
mkdir /usr/local/nginx/html/certbot
mkdir /usr/local/nginx/html/certbot-configs

# nginx.conf
mv /usr/local/nginx/conf/nginx.conf /usr/local/nginx/conf/nginx.conf.bak
cp /usr/local/src/fast-server/conf/nginx.conf /usr/local/nginx/conf/nginx.conf
mv /usr/local/src/fast-server/conf/vhosts /usr/local/nginx/conf/vhosts
# certbot
cp /usr/local/src/fast-server/conf/www.example.com.conf /usr/local/nginx/html/certbot/www.example.com.conf

chown -R webmaster:webmaster /usr/local/nginx

# install php

cd /usr/local/src/fast-server/${phpDir}

./configure --prefix=/usr/local/php \
--enable-fpm \
--with-fpm-user=webmaster \
--with-fpm-group=webmaster \
--disable-short-tags \
--enable-libxml \
--with-libxml-dir \
--with-openssl \
--with-openssl-dir \
--with-zlib \
--with-zlib-dir \
--with-pcre-dir \
--enable-bcmath \
--with-bz2 \
--enable-calendar \
--enable-ctype \
--with-curl \
--enable-exif \
--enable-fileinfo \
--enable-filter \
--enable-ftp \
--with-jpeg-dir \
--with-png-dir \
--with-webp-dir \
--with-freetype-dir \
--with-gd \
--enable-gd-native-ttf \
--with-gettext \
--with-gmp \
--with-mhash \
--with-iconv \
--with-iconv-dir \
--enable-json \
--enable-mbstring \
--with-mcrypt \
--enable-zip \
--with-xmlrpc \
--with-mysqli \
--with-pdo-mysql \
--enable-session \
--enable-opcache \
--enable-simplexml \
--enable-soap \
--enable-sockets \
--enable-wddx
#  --with-fpm-systemd      Activate systemd integration


make && make install

# php conf
cp /usr/local/src/fast-server/conf/php-fpm.conf  /usr/local/php/etc/php-fpm.conf
cp /usr/local/src/fast-server/conf/www.conf      /usr/local/php/etc/php-fpm.d/www.conf
cp /usr/local/src/fast-server/conf/php.ini       /usr/local/php/lib/php.ini

# yaconf
cd /usr/local/src/fast-server/yaconf${yaconfDir}
/usr/local/php/bin/phpize
./configure --with-php-config=/usr/local/php/bin/php-config
make && make install

# yac
cd /usr/local/src/fast-server/yac${yacDir}
/usr/local/php/bin/phpize
./configure --with-php-config=/usr/local/php/bin/php-config
make && make install

chown -R webmaster:webmaster /usr/local/php

# install percona-server 5.6

cd /usr/local/src/fast-server/${perconaDir}

cmake -DCMAKE_INSTALL_PREFIX=/usr/local/mysql \
-DSYSCONFDIR=/etc \
-DMYSQL_DATADIR=/usr/local/mysql/servdata \
-DTMPDIR=/usr/local/mysql/servtmp \
-DSYSTEMD_PID_DIR=/usr/local/mysql/servlog \
-DMYSQL_UNIX_ADDR=/tmp/mysql.sock \
-DENABLED_LOCAL_INFILE=1 \
-DDEFAULT_CHARSET=utf8mb4 \
-DDEFAULT_COLLATION=utf8mb4_unicode_ci \
-DMYSQL_TCP_PORT=3306 \
-DWITH_XTRADB_STORAGE_ENGINE=1 \
-DWITH_INNOBASE_STORAGE_ENGINE=1 \
-DWITH_ARCHIVE_STORAGE_ENGINE=1 \
-DWITH_BLACKHOLE_STORAGE_ENGINE=1 \
-DWITH_PERFSCHEMA_STORAGE_ENGINE=1 \
-DWITH_EXTRA_CHARSETS=all \
-DWITH_LIBEVENT=bundled \
-DWITH_SSL=bundled \
-DWITH_ZLIB=bundled

# not used: DSYSTEMD_PID_DIR DWITH_XTRADB_STORAGE_ENGINE

make && make install

mkdir /usr/local/mysql/servdata
mkdir /usr/local/mysql/servtmp
mkdir /usr/local/mysql/servlog

# mysql conf
mv /etc/my.cnf /etc/my.cnf.bakup
cp /usr/local/src/fast-server/conf/my-percona-56.cnf /etc/my.cnf

chown -R mysql:mysql /usr/local/mysql

# initialize mysql data @see https://dev.mysql.com/doc/refman/5.6/en/mysql-install-db.html
/usr/local/mysql/scripts/mysql_install_db --user=mysql \
--basedir=/usr/local/mysql \
--datadir=/usr/local/mysql/servdata

# add mysqld service
cp /usr/local/mysql/support-files/mysql.server /etc/init.d/mysqld
chmod +x /etc/init.d/mysqld

chown -R mysql:mysql /usr/local/mysql

# install redis
cd /usr/local/src/fast-server/${redisDir}
make && make PREFIX=/usr/local/redis install

mkdir /usr/local/redis/conf
cp /usr/local/src/fast-server/conf/redis.conf /usr/local/redis/conf/redis.conf

# swap off
swapoff /home/swap
rm -rf /home/swap

# start all service
/usr/local/nginx/sbin/nginx
/usr/local/php/sbin/php-fpm
service mysqld start

# access
chown -R webmaster:webmaster /usr/local/redis
chown -R webmaster:webmaster /usr/local/nginx
chown -R webmaster:webmaster /usr/local/php
chown -R mysql:mysql /usr/local/mysql

# password
/usr/local/mysql/bin/mysqladmin -uroot password abcd

# certbot
cd /usr/local/src/fast-server
wget https://github.com/certbot/certbot/archive/v${certbotVer}.zip
unzip v${certbotVer}.zip
mv certbot-${certbotVer} ${certbotDir}

chmod +x /usr/local/src/fast-server/${certbotDir}/certbot-auto

/usr/local/src/fast-server/${certbotDir}/certbot-auto
