# Fast-Server
Shell Script For Buliding High Performance Web Server On Aliyun ECS (Nginx + PHP + MySql + Redis).

# Usage
```shell
cd /usr/local/src
git clone https://github.com/zzzeep/fast-server.git
cd fast-server
chmod +x install-percona-5.6.x.sh
./install-percona-5.6.x.sh
```

# Notes
+ 1.Database's password for `root` user is abcd
+ 2.Don't forget to change all user's password in mysql.user
```shell
mysql -uroot -pYOURPASSWORD;
mysql> use mysql;
mysql> UPDATE user SET Password = PASSWORD('newpass') WHERE user = 'root'; (option 1)
mysql> SET PASSWORD FOR 'root'@'localhost' = PASSWORD('newpass'); (option 2)
mysql> FLUSH PRIVILEGES;
```

# Commands
### Nginx
```shell
/usr/local/nginx/sbin/nginx -t #test
/usr/local/nginx/sbin/nginx #start
/usr/local/nginx/sbin/nginx -s stop #stop
/usr/local/nginx/sbin/nginx -s reload #restart
```

### PHP-FPM
```shell
/usr/local/php/sbin/php-fpm #start
kill -INT `cat /usr/local/php/var/run/php-fpm.pid` #stop
kill -USR2 `cat /usr/local/php/var/run/php-fpm.pid` #restart

ps aux | grep -c php-fpm #current php-fpm process number
```

### MySQL
```shell
service mysqld start #start
service mysqld stop #stop
service mysqld restart #restart
```

### Redis
```shell
/usr/local/redis/bin/redis-server /usr/local/redis/conf/redis.conf #start
/usr/local/redis/bin/redis-cli shutdown #stop

/usr/local/redis/bin/redis-cli INFO | grep ^db #show databases
```

# Links
https://github.com/nginx/nginx

https://github.com/php/php-src

https://github.com/percona/percona-server
